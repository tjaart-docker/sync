FROM perl:latest

MAINTAINER Tjaart van der Walt <docker@tjaart.co.za>

RUN cpanm Carton
RUN apt-get -qq update
RUN  apt-get install -qq -y mysql-client
RUN printf "[mysql]\nuser=root\npassword=password\n\n[client]\nprotocol=tcp" > /etc/mysql/my.cnf
RUN rm -rf /var/lib/apt/lists/*
